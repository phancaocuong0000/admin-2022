const configuration = {

  ApiUrl: "https://backend-a3rc.onrender.com/",
  setApiRequestToken: (token) => {
    localStorage.setItem("requestToken", JSON.stringify(token));
  },
  removeApiRequestToken: (token) => {
    localStorage.removeItem("requestToken");
  },
  getAPIRequestToken: () => {
    return JSON.parse(localStorage.getItem("requestToken"));
  },
};

export const clientURL = "http://fe-siken-ute.ml/";

export default configuration;
